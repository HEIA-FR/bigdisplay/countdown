// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package countdown

import (
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

// Counter states
const (
	frozen   = iota
	counting = iota
	blinking = iota
)

// Countdown is the main structure to store countdown variables
type Countdown struct {
	count      int
	blinkCount int
	state      int
	mqtt       MQTT.Client
	topic      string
	clock      <-chan time.Time
	blink      <-chan time.Time
	refresh    <-chan time.Time
}

func (cd *Countdown) startCounting() {
	if cd.state != counting {
		cd.blink, cd.refresh = nil, nil
		cd.state = counting
		cd.clock = time.NewTicker(1 * time.Second).C
	}
}

func (cd *Countdown) startBlinking() {
	if cd.state != blinking {
		cd.clock, cd.refresh = nil, nil
		cd.state = blinking
		cd.blinkCount = 0
		cd.blink = time.NewTicker(500 * time.Millisecond).C
	}
}

func (cd *Countdown) freeze() {
	cd.blink, cd.clock = nil, nil
	cd.state = frozen
	cd.refresh = time.NewTicker(1 * time.Second).C
}

func (cd *Countdown) tick() {
	if cd.count == 0 {
		cd.startBlinking()
	} else {
		if cd.state == counting {
			cd.count--
		}
		cd.publish()
	}
}

func (cd *Countdown) blinkTick() {
	if cd.state == blinking {
		cd.blinkCount = 1 - cd.blinkCount
	}
}

func (cd *Countdown) toString() string {
	if cd.state == blinking {
		if cd.blinkCount == 0 {
			return "------"
		}
		return " 00.00 "
	}
	return fmt.Sprintf(" %02d.%02d ", (cd.count/60)%60, cd.count%60)
}

func (cd *Countdown) publish() {
	s := cd.toString()
	log.Debugf("Sending '%s' to topic %s", s, cd.topic)
	if token := cd.mqtt.Publish(cd.topic, 0, true, s); token.Wait() && token.Error() != nil {
		log.Warnf("Error sending message: %v", token.Error())
	}
}

// Connect connects the countdown to the publishing MQTT broker
func (cd *Countdown) Connect() error {
	token := cd.mqtt.Connect()
	token.Wait()
	cd.freeze()
	return token.Error()
}

// Disconnect disconnects the countdown from the publishing MQTT broker
func (cd *Countdown) Disconnect() {
	cd.mqtt.Disconnect(250)
}

// Loop is gthe main processing loop for the timer. It receives commands from the command channel of strings
func (cd *Countdown) Loop(command chan string) { // nolint: gocyclo
	for {
		select {
		case <-cd.clock:
			cd.tick()
		case c := <-command:
			switch c {
			case "stop", "freeze":
				cd.freeze()
			case "run", "go":
				cd.startCounting()
			default:
				if i, err := strconv.Atoi(c); err == nil {
					cd.count = i
					if cd.state == blinking {
						cd.startCounting()
					}
					cd.publish()
				}
			}
		case <-cd.blink:
			cd.publish()
			cd.blinkTick()
		case <-cd.refresh:
			cd.publish()
		}
	}
}

// NewCountdown creates a new countdown structure with broker and topic information for the MQTT publishing broker
func NewCountdown(broker, topic string) *Countdown {
	opts := MQTT.NewClientOptions()
	opts.AddBroker(broker)
	return &Countdown{
		count: 0,
		mqtt:  MQTT.NewClient(opts),
		topic: topic,
	}
}
