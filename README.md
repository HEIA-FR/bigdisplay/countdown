[![GoDoc](https://godoc.org/gitlab.com/HEIA-FR/bigdisplay/countdown?status.svg)](https://godoc.org/gitlab.com/HEIA-FR/bigdisplay/countdown)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/HEIA-FR/bigdisplay/countdown)](https://goreportcard.com/report/gitlab.com/HEIA-FR/bigdisplay/countdown)
[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://gitlab.com/HEIA-FR/bigdisplay/countdown)

# BigDisplay Countdown
