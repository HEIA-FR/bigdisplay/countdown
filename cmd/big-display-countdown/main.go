// Copyright 2018 Jacques Supcik
// Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	log "github.com/sirupsen/logrus"
	"gitlab.com/heia-fr/bigdisplay/countdown"
)

const (
	topic = "countdown.command"
)

var command = make(chan string)

func main() {
	debug := flag.Bool("debug", false, "Run in debug mode")
	verbose := flag.Bool("verbose", false, "Run in verbose mode")

	commandBroker := flag.String("command-broker", "127.0.0.1:1883", "MQTT Command Broker")
	commandTopic := flag.String("command-topic", "command", "MQTT Command Topic")

	displayBroker := flag.String("display-broker", "127.0.0.1:1883", "MQTT Display Broker")
	displayTopic := flag.String("display-topic", "message", "MQTT Display Topic")

	flag.Parse()

	switch {
	case *debug:
		log.SetLevel(log.DebugLevel)
	case *verbose:
		log.SetLevel(log.InfoLevel)
	default:
		log.SetLevel(log.WarnLevel)
	}

	// connect to MQTT command broker
	opts := MQTT.NewClientOptions()
	opts.AddBroker(*commandBroker)

	opts.SetDefaultPublishHandler(func(client MQTT.Client, msg MQTT.Message) {
		command <- string(msg.Payload())
	})
	client := MQTT.NewClient(opts)
	log.Debug("Connecting to MQTT Broker")
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}
	defer client.Disconnect(250)
	if token := client.Subscribe(*commandTopic, 0, nil); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}

	// create new countdown
	cd := countdown.NewCountdown(*displayBroker, *displayTopic)
	if err := cd.Connect(); err != nil {
		log.Fatalf("Error creating counter %v", err)
	}
	defer cd.Disconnect()
	go cd.Loop(command)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
}
