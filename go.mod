module gitlab.com/heia-fr/bigdisplay/countdown

require (
	github.com/eclipse/paho.mqtt.golang v1.1.1
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/sirupsen/logrus v1.1.1
	golang.org/x/crypto v0.0.0-20181025213731-e84da0312774 // indirect
	golang.org/x/net v0.0.0-20181023162649-9b4f9f5ad519 // indirect
	golang.org/x/sys v0.0.0-20181026064943-731415f00dce // indirect
)
